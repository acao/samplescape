

## Order of initialization
wrapped in `terminus` sigint/sigterm handler for docker/kubernetes:
- serial (async/await)

1. Logger - `logger` export for custom logger, otherwise `pino`
1. Env config validate (envalid)
1. Client service connections (async) - healthchecks?
1. Migrations if `./migrations` exists (version argument from init) after db
1. Execute service specific initialization middleware as provided - a listener for a worker and/or graphql-yoga and async utiliity for resolving the resolver context. 
   - Each service will provide an export of `init` that returns a promise, and injects everthing from above into the arguments :D. 
   - there will also be a `status` and `version` export you can optionally specify


## Methods of initialization

### CLI

`@samplescape/drawings` exports `init`, `status` and `version`

```sh
service-init @samplescape/drawings
service-init @samplescape/drawings --version=x.x
```


### Progromatically
