const terminus = require('@godaddy/terminus')
const serviceLogger = require('pino')
const pkgConfig = require('pkg-config')

const terminusOptions = {
  // healtcheck options
  healthChecks: {
    '/healthcheck': healthCheck    // a promise returning function indicating service health
  },
 
  // cleanup options
  timeout: 1000,                   // [optional = 1000] number of milliseconds before forcefull exiting
  signal,                          // [optional = 'SIGTERM'] what signal to listen for relative to shutdown
  signals,                         // [optional = []] array of signals to listen for relative to shutdown
  beforeShutdown,                  // [optional] called before the HTTP server starts its shutdown
  onSignal,                        // [optional] cleanup function, returning a promise (used to be onSigterm)
  onShutdown,                      // [optional] called right before exiting
  onSendFailureDuringShutdown,     // [optional] called before sending each 503 during shutdowns
 
  // both
  logger                           // [optional] logger function to be called with errors
};

async function initService({ logger, service, name, args }) {
  const initLogger = logger.child({ module: `${name}.init`})
  try {
    if(service.getConfig) {
      throw Error('need get config function')
    }
    const svcConfig = await service.getConfig(args)
    svcConfig.args = args
    if (svcConfig.getEnvVars) {
      svcConfig.env = envalid.cleanEnv(process.env, svcConfig.getEnvVars(envalid, svcConfig))
    }
  }
  catch(err) {
    initLogger.error(err)
    throw err
  }

}


module.exports = async function getService(name, args) {
  const isInDeps = !!pkgConfig().dependencies[name]
  if (!isInDeps) {
    throw Error(`Module ${name} is not in the package.json, thus not executable by this script`)
  }
  else {
    const service = require(name)
    const logger = require(name).logger || serviceLogger({ module: name })
    return terminus(initService({ logger, service, name, args }), { logger })
  }
 
}
