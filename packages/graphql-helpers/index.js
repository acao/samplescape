const { NatsPubSub } = require('@moonwalker/graphql-nats-subscriptions')

module.exports = {
  NatsPubSub
}