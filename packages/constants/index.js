module.exports.DB_NAME = 'samplescape'
module.exports.CHANGE_FEED_NAME = 'changefeed' // random channel name
module.exports.INITIAL_TABLES = ['drawings', 'iterations', 'accounts', 'teams']
module.exports.SERVICES = [
  {
    type: 'web',
    name: 'web',
    package: '@samplescape/web'
  },
  {
    type: 'graphql',
    name: 'drawings',
    package: '@samplescape/drawings'
  },
  {
    type: 'graphql',
    name: 'changefeed',
    package: '@samplescape/changefeed',
    multiTenant: true
  },
  {
    type: 'graphql',
    name: 'gateway',
    package: '@samplescape/gateway'
  }
]