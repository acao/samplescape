const { NatsPubSub } = require('@moonwalker/graphql-nats-subscriptions')

// const options = {
//   host: 'redis',
//   port: REDIS_PORT,
//   retry_strategy: options => {
//     // reconnect after
//     return Math.max(options.attempt * 100, 3000);
//   }
// }

const pubsubClient = () => {
  try {
    const pubsub = new NatsPubSub({
      servers: ['nats://nats:4222'],
    })
    return pubsub
  }
  catch(err) {
    throw err
  }
  finally {
    console.log('pubsub connected')
  }
}

module.exports = {
  pubsubClient
}