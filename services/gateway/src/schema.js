const { HttpLink } = require('apollo-link-http');
const fetch = require('node-fetch');
const fs = require('fs')
const path = require('path')
const { introspectSchema, makeRemoteExecutableSchema } = require('graphql-tools')

const { SERVICES } = require('@samplescape/constants')

export async function getRemoteSchema(config) {
  const link = new HttpLink({ uri: `http://${config.name}:8080`, fetch });
  const schema = await introspectSchema(link);

  const executableSchema = makeRemoteExecutableSchema({
    schema,
    link,
  });

  return executableSchema
}

export async function getLocalSchema(config) {
  const schemaPath = path.join(config.package, 'schema.graphql')
  const modulePath = path.join(config.package)
  try {
    if (require.resolve(schemaPath) && require.resolve(modulePath)) {
      const { resolvers } = await import(config.package)
    return makeExecutableSchema({
      typeDefs: await import(schemaPath),
      resolvers,
    })
    }
    else {
      throw Error(`Invalid schema or module resolution for ${config}`)
    }
  }
  catch(err) {
    throw err
  }
}

export async function getRemoteSchemas(multiTenant = false) {
  const schemas = await Promise.all(SERVICES
    .filter(({ type, name }) => type === 'graphql' && name !=='gateway')
    .map(svcConfig => {
      return svcConfig.multiTenant ? getRemoteSchema(svcConfig) : getLocalSchema(svcConfig)
    }))
    return mergeSchemas({ schemas })
} 