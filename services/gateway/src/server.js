const { GraphQLServer } = require('graphql-yoga')

const { getRemoteSchemas } = require('./schema')



async function getServer() {
  try {
    const schema = await getRemoteSchemas()

    return new GraphQLServer({
      schema
    })
  } catch (err) {
    throw err
  }
}

getServer()
  .then(server => {
    server.start(({ port }) => console.log(`Server is running on localhost:${port}`))
  })
  .catch(err => console.error(err))
