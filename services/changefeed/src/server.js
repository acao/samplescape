const { GraphQLServer } = require('graphql-yoga')
const { withFilter } = require('graphql-subscriptions')
const { CHANGE_FEED_NAME } = require('@samplescape/constants')
const { pubsubClient } = require('@samplescape/message-helpers')

const resolvers = {
  Query: {
    status: () => true
  },
  Subscription: {
    changefeed: {
      subscribe: (parent, args, { pubsub }) => {
        try {
            // pubsub.subscribe(CHANGE_FEED_NAME, (msg) => {
            //   console.log(msg)
            // })
            console.log({ CHANGE_FEED_NAME })
            return pubsub.asyncIterator(CHANGE_FEED_NAME)
          }
          catch(err) {
            throw err
          }

      }
    }
  }
}

async function getServer() {
  const pubsub = pubsubClient()
  try {
    return new GraphQLServer({
      typeDefs: `${__dirname}/schema.graphql`,
      resolvers,
      context: { pubsub }
    })
  } catch (err) {
    console.log(err)
    throw err
  }
}

getServer()
  .then(server => {
    server.start(({ port }) => console.log(`Server is running on localhost:${port}`))
  })
  .catch(err => console.error(err))
