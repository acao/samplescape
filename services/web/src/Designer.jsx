import React from "react";
import Designer from "@experium/react-designer";
import { data } from "./art";

class SampleDesigner extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    data
  };

  handleUpdate(data) {
    this.setState({ data });
  }

  render() {
    return (
      <Designer
        height={500}
        width={500}
        onUpdate={this.handleUpdate.bind(this)}
        data={this.state.data}
      />
    );
  }
}
export default SampleDesigner;
