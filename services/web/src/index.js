import React from "react";
import ReactDOM from "react-dom";
import SampleDesigner from "./Designer";
import "./styles.css";

window._ = require('underscore');

function App() {
  return (
    <div className="App" style={{ marginLeft: 40 }}>
      <SampleDesigner />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
