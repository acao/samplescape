const { GraphQLServer } = require('graphql-yoga')
const { pubsubClient } = require('@samplescape/message-helpers')
const { DB_NAME, CHANGE_FEED_NAME, INITIAL_TABLES } = require('@samplescape/constants')
const isProd = process.env.NODE_ENV === 'production'
const nanoid = require('nanoid')

const rethinkdb_init = require('rethinkdb-nodash')

const resolvers = {
  Query: {
    drawing: (parent, id, { r }) => r.get(id).run(r.conn)
  },
  Mutation: {
    draw: async (parent, args, { pubsub, r }) => {
      const data = args.data
      let result
      if (data && !data.id) {
        data.id = nanoid()
        const insertOp =  await r.drawings.insert(data, { returnChanges: 'always' }).run()
        result = insertOp.changes[0].new_val
      } else {
        const insertOp = await r.drawings.get(data.id).update(data, { returnChanges: 'always' }).run()
        result = insertOp.changes[0].new_val
      }
      const msgResult = await pubsub.publish(CHANGE_FEED_NAME, { data: result})
      console.log({ msgResult, CHANGE_FEED_NAME, result })
      return result
    }
  }
}

async function getServer() {
  try {
    const db = await rethinkdb_init({
      host: isProd ? 'database' : 'localhost',
      port: 28015,
      db: DB_NAME,
      reconnect_interval: 5000 // this is the default
    })

   const pubsub = pubsubClient()

    const dbTables = INITIAL_TABLES.reduce((acc, table) => {
      acc[table] = db.table(table)
      return acc
    }, {})

    return new GraphQLServer({
      typeDefs: `${__dirname}/schema.graphql`,
      resolvers,
      context: () => {
        if (db) {
          console.log('db connected')
        }
        return {
         pubsub,
          r: {
            conn: db,
            ...dbTables
          }
        }
      }
    })
  } catch (err) {
    console.log(err)
    throw err
  }
}

getServer()
  .then(server => {
    server.start(({ port }) => console.log(`Server is running on localhost:${port}`))
  })
  .catch(err => console.error(err))
