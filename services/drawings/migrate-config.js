module.exports = {
  db: "samplescape",
  driver: "rethinkdbdash",
  pool: true,
  servers: [
    { host: "localhost", port: 28015 }
    // { host: "localhost", port: 28016 }
  ],
  ssl: false
}