const { DB_NAME, INITIAL_TABLES } = require('@samplescape/constants')

exports.up = async function (r, connection) {
  return Promise.all(INITIAL_TABLES.map(table => r.tableCreate(table)))
}

exports.down = async function (r, connection) {
 
}
